﻿using Reversi.Players;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Reversi.Test
{
    public class AiPlayerTest
    {
        [Fact]
        public async Task Should_Place_Into_Allowed_Location()
        {
            var aiPlayer = new SimpleAiPlayer(PlayerColor.White);
            var placementOptions = new List<Vector2>
            {
                new Vector2(0,0),
                new Vector2(4, 7),
                new Vector2(3, 4)
            };

            var playerInput = await aiPlayer.GetInputAsync(placementOptions);

            Assert.Contains(playerInput, placementOptions);
        }
    }
}
