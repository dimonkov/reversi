using Reversi.Placement;
using Reversi.Players;
using Xunit;

namespace Reversi.Test
{
    public class PlacementStrategyTest
    {
        [Fact]
        public void Should_Be_Able_To_Place_Horizontal()
        {
            var board = new Board(8);
            var horizontal = new HorizontalPlacementStrategy(board);

            board.SetElementAt(0, 0, PlayerColor.White);
            board.SetElementAt(1, 0, PlayerColor.Black);

            Assert.True(horizontal.CanPlace(new Vector2(2, 0), PlayerColor.White));
        }

        [Fact]
        public void Should_Be_Able_To_Place_Vertical()
        {
            var board = new Board(8);
            var horizontal = new VerticalPlacementStrategy(board);

            board.SetElementAt(0, 0, PlayerColor.White);
            board.SetElementAt(0, 1, PlayerColor.Black);

            Assert.True(horizontal.CanPlace(new Vector2(0, 2), PlayerColor.White));
        }

        [Fact]
        public void Should_Be_Able_To_Place_Diagonal()
        {
            var board = new Board(8);
            var horizontal = new DiagonalPlacementStrategy(board);

            board.SetElementAt(0, 0, PlayerColor.White);
            board.SetElementAt(1, 1, PlayerColor.Black);

            Assert.True(horizontal.CanPlace(new Vector2(2, 2), PlayerColor.White));
        }

        [Fact]
        public void Should_Reverse_Enemy_Tile()
        {
            var board = new Board(8);
            
            board.SetElementAt(0, 0, PlayerColor.Black);
            board.SetElementAt(1, 1, PlayerColor.White);
            var horizontal = new DiagonalPlacementStrategy(board);
            
            horizontal.PiecePlaced(new Vector2(2,2),PlayerColor.Black);
            var elem = board.GetElementAt(1, 1);
            
            Assert.Equal(PlayerColor.Black,elem);
        }
    }
}
