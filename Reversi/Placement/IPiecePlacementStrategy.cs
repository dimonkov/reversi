using System.Collections.Generic;
using Reversi.Players;

namespace Reversi.Placement
{
    public interface IPiecePlacementStrategy
    {
        ICollection<Vector2> GetAvailablePiecePlacements(Board board, PlayerColor playerColor);

        void PiecePlaced(Vector2 coordinates, PlayerColor color);
    }
}

