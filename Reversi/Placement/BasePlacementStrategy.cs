using System;
using System.Collections.Generic;
using System.Linq;
using Reversi.Players;

namespace Reversi.Placement
{
    public abstract class BasePlacementStrategy : IPiecePlacementStrategy
    {
        private readonly Board _board;

        public BasePlacementStrategy(Board board)
        {
            _board = board;
        }
        protected abstract ICollection<Func<IEnumerable<Vector2>>> GetDirectionIterators(Vector2 coord);
        public ICollection<Vector2> GetAvailablePiecePlacements(Board board, PlayerColor playerColor)
        {
            List<Vector2> placements = new List<Vector2>();
            for (int y = 0; y < board.Side; y++)
            {
                for (int x = 0; x < _board.Side; x++)
                {
                    var coord = new Vector2 {X = x, Y = y};
                    if (CanPlace(coord, playerColor))
                        placements.Add(coord);
                }
            }

            return placements;
        }

        public void PiecePlaced(Vector2 coordinates, PlayerColor color)
        {
            _board.SetElementAt(coordinates, color);

            foreach (var direction in GetDirectionIterators(coordinates))
            {
                var anchor = FindAnchor(coordinates, color, direction);
                
                if (anchor != null)
                    FillDirection(coordinates, anchor.Value, direction, color);
            }
        }

        public bool CanPlace(Vector2 coord, PlayerColor color)
        {
            var val = _board.GetElementAt(coord);
            if (val.HasValue)
                return false;
            
            foreach (var direction in GetDirectionIterators(coord))
            {
                var anchor = FindAnchor(coord, color, direction);
                if (anchor.HasValue && coord != anchor)
                    return true;
            }
            return false;
        }

        private void FillDirection(Vector2 coord, Vector2 anchor, Func<IEnumerable<Vector2>> direction,
            PlayerColor color)
        {
            _board.SetElementAt(coord, color);
            foreach (var directionPoint in direction.Invoke())
            {
                _board.SetElementAt(directionPoint, color);
                
                if (directionPoint == anchor)
                    return;
            }
        }
            
        private Vector2? FindAnchor(Vector2 coord, PlayerColor color, Func<IEnumerable<Vector2>> direction)
        {
            Vector2? last = null;
            foreach (var directionPoint in direction.Invoke())
            {
                if (!_board.IsAValidCoord(directionPoint.X, directionPoint.Y))
                    return null;

                var valueAt = _board.GetElementAt(directionPoint);

                if (!valueAt.HasValue)
                    return null;

                if (valueAt.Value == color)
                    return last;

                last = directionPoint;
            }

            return null;
        }
        
        public IEnumerable<T> Sequence<T>(T item, Func<T, T> transform)
        {
            while (true)
            {
                item = transform.Invoke(item);
                yield return item;
            }
        }
    }
}