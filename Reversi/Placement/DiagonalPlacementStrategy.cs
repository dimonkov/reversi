using System;
using System.Collections.Generic;

namespace Reversi.Placement
{
    public class DiagonalPlacementStrategy : BasePlacementStrategy
    {
        public DiagonalPlacementStrategy(Board board) : base(board)
        {
        }

        protected override ICollection<Func<IEnumerable<Vector2>>> GetDirectionIterators(Vector2 coord)
        {
            return new List<Func<IEnumerable<Vector2>>>
            {
                () => Sequence(coord, vector2 => new Vector2
                {
                    X = vector2.X - 1,
                    Y = vector2.Y - 1
                }),
                () => Sequence(coord, vector2 => new Vector2
                {
                    X = vector2.X - 1,
                    Y = vector2.Y + 1
                }),
                () => Sequence(coord, vector2 => new Vector2
                {
                    X = vector2.X + 1,
                    Y = vector2.Y - 1
                }),
                () => Sequence(coord, vector2 => new Vector2
                {
                    X = vector2.X + 1,
                    Y = vector2.Y + 1
                })
            };
        }
    }
}