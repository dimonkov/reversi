using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Reversi.Placement;
using Reversi.Players;

namespace Reversi
{
    public class Game
    {
        private readonly Func<PlayerColor, IPlayer> _player1Factory;
        private readonly Func<PlayerColor, IPlayer> _player2Factory;

        public IPlayer Player1 { get; protected set; }
        
        public IPlayer Player2 { get; protected set; }
        
        public Board Board { get; protected set; }

        public int CurrentTurn { get; protected set; }
        
        public ICollection<IPiecePlacementStrategy> PiecePlacementStrategies { get; }

        public Game(Func<PlayerColor, IPlayer> player1Factory,
                    Func<PlayerColor, IPlayer> player2Factory,
                    Board board, ICollection<IPiecePlacementStrategy> piecePlacementStrategies)
        {
            _player1Factory = player1Factory;
            _player2Factory = player2Factory;
            Board = board;
            PiecePlacementStrategies = piecePlacementStrategies;
        }
        
        public Task ResetAsync(CancellationToken cancellationToken = default)
        {
            Board.Reset();
            
            // TODO: Introduce randomisation
            Player1 ??= _player1Factory.Invoke(PlayerColor.White);
            
            Player2 ??= _player2Factory.Invoke(PlayerColor.Black);

            CurrentTurn = 0;

            return Task.WhenAll(Player1.OnBoardStateChangedAsync(Board, cancellationToken), 
                Player2.OnBoardStateChangedAsync(Board, cancellationToken));
        }

        public async Task<TurnResult> TurnAsync(CancellationToken cancellationToken = default)
        {
            var player = GetPlayerForThisTurn();

            var col = player.PlayerColor;

            var placementOptions = PiecePlacementStrategies
                .SelectMany(strat => strat.GetAvailablePiecePlacements(Board, col))
                .Distinct()
                .ToArray();
            
            if (placementOptions.Length > 0)
            {
                try
                {
                    var input = await player.GetInputAsync(placementOptions, cancellationToken);
                    
                    
                    if (placementOptions.Contains(input))
                        foreach (var strategy in PiecePlacementStrategies)
                        {
                            strategy.PiecePlaced(input, col);
                        }
                }
                catch (TaskCanceledException)
                { 
                    // Ignore
                }

                await Task.WhenAll(Player1.OnBoardStateChangedAsync(Board, cancellationToken),
                    Player2.OnBoardStateChangedAsync(Board, cancellationToken));
            }
            else
            {
                var opponent = GetOpponentForThisTurn();
                var opponentPlayerCanMove = PiecePlacementStrategies.Sum(
                                                strategy => strategy.GetAvailablePiecePlacements(Board, opponent.PlayerColor).Count) != 0;
                
                if (!opponentPlayerCanMove)
                    return new TurnResult
                    {
                        GameStatus = GameStatus.Ended,
                        GameScore = GetGameScore()
                    };
            }

            CurrentTurn++;
            
            return new TurnResult
            {
                // TODO: fill in actual game status
                GameStatus = GameStatus.Running,
                GameScore = GetGameScore()
            };
        }

        private GameScore GetGameScore()
        {
            return new GameScore
            {
                Player1Score = Board.GetScore(Player1.PlayerColor),
                Player2Score = Board.GetScore(Player2.PlayerColor)
            };
        }

        private IPlayer GetPlayerForThisTurn()
        {
            return CurrentTurn % 2 == 0 ? Player1 : Player2;
        }
        
        private IPlayer GetOpponentForThisTurn()
        {
            return CurrentTurn % 2 == 1 ? Player1 : Player2;
        }
    }
}