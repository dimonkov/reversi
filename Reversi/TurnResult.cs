namespace Reversi
{
    public class TurnResult
    {
        public GameStatus GameStatus { get; set; }
        
        public GameScore GameScore { get; set; }
    }

    public enum GameStatus
    {
        Running,
        Ended
    }
}