using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Reversi.Players
{
    public interface IPlayer
    {
        PlayerColor PlayerColor { get; }
        
        Task OnBoardStateChangedAsync(Board board, CancellationToken cancellationToken = default);

        Task<Vector2> GetInputAsync(ICollection<Vector2> placementOptions, CancellationToken cancellationToken = default);
    }
}