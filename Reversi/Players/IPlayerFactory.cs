namespace Reversi.Players
{
    public interface IPlayerFactory
    {
        IPlayer GetPlayer(PlayerType playerType, PlayerColor playerColor);
    }

    public enum PlayerType
    {
        Console,
        AI,
        RemotePlayer
    }
}