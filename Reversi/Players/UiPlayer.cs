using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Reversi.Players
{
    public class UiPlayer : IPlayer
    {
        public UiPlayer(PlayerColor playerColor, IBoardView view)
        {
            PlayerColor = playerColor;
            _view = view;
        }

        public PlayerColor PlayerColor { get; }

        private Board _board;

        private IBoardView _view;

        public async Task OnBoardStateChangedAsync(Board board, 
            CancellationToken cancellationToken = default) => _board = board;
        

        public async Task<Vector2> GetInputAsync(ICollection<Vector2> placementOptions, CancellationToken cancellationToken = default)
        {
            await _view.PresentBoardAsync(_board, placementOptions, cancellationToken);

            return await _view.GetInputAsync(placementOptions, cancellationToken);
        }
    }
}