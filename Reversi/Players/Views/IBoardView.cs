using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Reversi.Players
{
    public interface IBoardView
    {
        Task<Vector2> GetInputAsync(ICollection<Vector2> placementOptions,
            CancellationToken cancellationToken = default);

        Task PresentBoardAsync(Board board, ICollection<Vector2> placementOptions, 
            CancellationToken cancellationToken =default);
    }
}