using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Reversi.Players
{
    public class ConsoleView : IBoardView
    {
        public async Task<Vector2> GetInputAsync(ICollection<Vector2> placementOptions,
            CancellationToken cancellationToken = default)
        {
            while (true)
            {
                Console.WriteLine("Input next position: ");
                var xStr = Console.ReadLine();
                var yStr = Console.ReadLine();
                if (int.TryParse(xStr, out var x) && int.TryParse(yStr, out var y))
                    return new Vector2(x, y);
                
                Console.WriteLine("Invalid!");
            }
        }
        
        public async Task PresentBoardAsync(Board board, ICollection<Vector2> placementOptions, 
            CancellationToken cancellationToken = default)
        {
            PrintHeader((int)board.Side);

            for (int i = 0; i < board.Side; i++)
            {
                PrintRow(board, placementOptions, i);
            }
        }
        
        private void PrintHeader(int side)
        {
            Console.WriteLine("-----------------------------------------------------------------");
            Console.Write("\t");
            for (int i = 0; i < side; i++)
            {
                Console.Write($"{i}\t");
            }
            Console.WriteLine();
        }

        private void PrintRow(Board board, ICollection<Vector2> placementOptions,int row)
        {
            Console.Write($"{row}\t");
            for (int i = 0; i < board.Side; i++)
            {
                var ch = placementOptions.Contains(new Vector2(i, row)) ? '~' : SlotToChar(board.GetElementAt(i, row));
                Console.Write($"{ch}\t");
            }
            Console.WriteLine();
        }

        private char SlotToChar(PlayerColor? pk)
        {
            return pk switch
            {
                null => ' ',
                PlayerColor.Black => '@',
                PlayerColor.White => 'O',
                _ => ' '
            };
        }
    }
}