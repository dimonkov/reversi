using System;

namespace Reversi.Players
{
    public class TypedPlayerFactory : IPlayerFactory
    {
        public IPlayer GetPlayer(PlayerType playerType, PlayerColor playerColor)
        {
            return playerType switch
            {
                // Switch expression syntax compiler is dumb,
                // it picks the type from the first lambda
                // and cannot see that we implement the same interface on both types
                PlayerType.Console => (IPlayer)new UiPlayer(playerColor, new ConsoleView()),
                PlayerType.AI => new SimpleAiPlayer(playerColor),
                _ => throw new NotSupportedException($"Player type: {playerType} is not yet supported")
            };
        }
    }
}