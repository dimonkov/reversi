using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Reversi.Players
{
    public class SimpleAiPlayer : IPlayer
    {
        // TODO: Maybe iinject?
        Random _random = new Random();
        
        public SimpleAiPlayer(PlayerColor playerColor)
        {
            PlayerColor = playerColor;
        }

        public PlayerColor PlayerColor { get; }
        public Task OnBoardStateChangedAsync(Board board, CancellationToken cancellationToken = default) => Task.CompletedTask;

        public Task<Vector2> GetInputAsync(ICollection<Vector2> placementOptions, CancellationToken cancellationToken = default)
        {
            var index = _random.Next(0, placementOptions.Count);
            return Task.FromResult(placementOptions.ElementAt(index));
        }
    }
}