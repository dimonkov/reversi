namespace Reversi
{
    public class GameScore
    {
        public int Player1Score { get; set; }
        
        public int Player2Score { get; set; }
    }
}