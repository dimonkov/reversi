namespace Reversi
{
    public struct Vector2
    {
        public int X;
        public int Y;

        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2))
                return false;

            return this == (Vector2)obj;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                hash = hash * 23 + X;
                hash = hash * 23 + Y;
                return hash;
            }
        }

        public static bool operator ==(Vector2 lhs, Vector2 rhs)
            => lhs.X == rhs.X && lhs.Y == rhs.Y;
        
        public static bool operator !=(Vector2 lhs, Vector2 rhs)
            => !(lhs == rhs);
    }
}