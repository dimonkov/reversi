﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Reversi.Placement;
using Reversi.Players;

namespace Reversi
{
    internal class Program
    {
        public static async Task Main(string[] args)
        {
            var playerFactory = new TypedPlayerFactory();
            var board = new Board(8);
            
            var game = new Game(color => playerFactory.GetPlayer(PlayerType.Console, color),
                color => playerFactory.GetPlayer(PlayerType.AI, color),
                board,
                new List<IPiecePlacementStrategy>
                {
                    new HorizontalPlacementStrategy(board),
                    new VerticalPlacementStrategy(board),
                    new DiagonalPlacementStrategy(board)
                } );

            await game.ResetAsync();

            var turnResult = new TurnResult
            {
                GameStatus = GameStatus.Running
            };
            
            while (turnResult.GameStatus == GameStatus.Running)
            {
                turnResult = await game.TurnAsync();
            }

            Console.WriteLine($"Game ended");
            Console.WriteLine("Results: ");
            Console.WriteLine($"Player1: {turnResult.GameScore.Player1Score}");
            Console.WriteLine($"Player2: {turnResult.GameScore.Player2Score}");
        }
    }
}