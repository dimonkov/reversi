using System;
using System.Collections.Generic;
using System.Linq;
using Reversi.Players;

namespace Reversi
{
    public class Board
    {
        public uint Side { get; }

        public IEnumerable<PlayerColor?> GetBoardSlots => _board;
        
        private List<PlayerColor?> _board;
        public Board(uint side)
        {
            if (side % 2 == 1)
                throw new ArgumentException("Board side must be divisible by 2", nameof(side));
            
            Side = side;
            _board = new List<PlayerColor?>(new PlayerColor?[side * side]);
            
            Reset();
        }

        public void Reset()
        {
            var center = (int)Side / 2;
            SetElementAt(center - 1, center - 1, PlayerColor.White);
            SetElementAt(center, center - 1, PlayerColor.Black);
            SetElementAt(center - 1, center, PlayerColor.Black);
            SetElementAt(center, center, PlayerColor.White);
        }

        public PlayerColor? GetElementAt(Vector2 coord)
            => GetElementAt(coord.X, coord.Y);

        public PlayerColor? GetElementAt(int x, int y)
            => _board[y * (int)Side + x];

        public void SetElementAt(Vector2 coord, PlayerColor? elem)
            => SetElementAt(coord.X, coord.Y, elem);

        public void SetElementAt(int x, int y, PlayerColor? elem)
            => _board[y * (int) Side + x] = elem;

        public int GetScore(PlayerColor playerColor)
            => _board.Count(slot => slot.HasValue && slot.Value == playerColor);

        public bool IsAValidCoord(int x, int y)
        {
            return x >= 0 && x < Side && y >= 0 && y < Side;
        }
    }
}